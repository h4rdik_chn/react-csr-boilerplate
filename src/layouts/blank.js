import React from "react";
import { Route } from "react-router-dom";

const HomeLayout = ({ children, ...rest }) => {
  return <div className="dashboardContainer">{children}</div>;
};

const HomeLayoutRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={matchProps => (
        <HomeLayout>
          <Component {...matchProps} />
        </HomeLayout>
      )}
    />
  );
};

export default HomeLayoutRoute;
