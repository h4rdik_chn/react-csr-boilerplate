import React, { Component } from "react";
import { Route } from "react-router-dom";
import Header from "../components/common/header";
import SideBar from "../components/common/sidebar";
const SideBarLayout = ({ children, ...rest }) => {
  return (
    <div className="dashboardContainer">
      <Header />
      <SideBar />
      <div className="dashboardContainer-inner">
        <section
          id="dashboard-rightContainer"
          className="dashboard-manageSales"
        >
          {children}
        </section>
      </div>
    </div>
  );
};

const SideBarLayoutRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={matchProps => (
        <SideBarLayout>
          <Component {...matchProps} />
        </SideBarLayout>
      )}
    />
  );
};

export default SideBarLayoutRoute;
