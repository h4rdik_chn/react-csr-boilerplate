import React from "react";
import ReactDOM from "react-dom";
import "./assets/css/index.scss";
// import App from './components/Home/App';
import * as serviceWorker from "./serviceWorker";
import { store } from "./redux/store";
import { Provider } from "react-redux";
import Routes from "./routeConfig";

ReactDOM.render(
  <Provider store={store}>
    <Routes />
    {/* <Router history={history}>
      <Switch>
        {routes.map(route => {
          if (route.hasOwnProperty("render") && typeof render === "function") {
            return (
              <Route
                path={route.path}
                render={route.render}
                exact={route.exact || false}
              />
            );
          } else {
            return (
              <Route
                path={route.path}
                component={route.component}
                exact={route.exact || false}
              />
            );
          }
        })}
      </Switch>
    </Router> */}
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
