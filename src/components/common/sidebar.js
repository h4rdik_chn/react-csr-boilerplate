import React, { Component } from "react";
import "../../assets/css/App.scss";
import Logo from "../../assets/images/logo.png";
import { history } from "../../history";

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classOpenMenu: false
    };
  }
  openMenu = () => {
    this.setState({
      classOpenMenu: !this.state.classOpenMenu
    });
  };

  //   closeMenu = () => {
  //     this.setState({
  //       classOpenMenu: false
  //     });
  //   };

  render() {
    let { classOpenMenu } = this.state;
    return (
      <div className={classOpenMenu ? "side_menu active" : "side_menu"}>
        {!classOpenMenu ? (
          <div class="open_menu" onClick={this.openMenu}>
            <span></span>
            <span></span>
            <span></span>
          </div>
        ) : null}
        <>
          <ul>
            <li className="menu_itms active">
              <a>
                {/* <img src="http://accounting.affleprojects.com/images/icon1.png" /> */}
                REPORT
                {/* <img
                src="http://accounting.affleprojects.com/images/menu_drop.png"
                className="menu_drop"
              /> */}
              </a>
              {/* <ul className="sub_menu">
              <li>
                <a
                  className
                  href="http://accounting.affleprojects.com/sales-revenue-report?month=06"
                >
                  Sales Revenue Report
                </a>
              </li>
            </ul> */}
            </li>
            <li className="menu_itms">
              <a>
                {/* <img src="http://accounting.affleprojects.com/images/icon4.png" /> */}
                SUMMARY
                {/* <img
                src="http://accounting.affleprojects.com/images/menu_drop.png"
                className="menu_drop"
              /> */}
              </a>
              <ul className="sub_menu">
                <li>
                  <a
                    className
                    href="http://accounting.affleprojects.com/advertiser-summary"
                  >
                    Advertiser Summary
                  </a>
                </li>
              </ul>
            </li>
          </ul>
          <div className="close_menu" onClick={this.openMenu}>
            <img src="http://accounting.affleprojects.com/images/close_menu.png" />
          </div>
        </>
      </div>
    );
  }
}

export default SideBar;
