import React, { Component } from "react";
import "../../assets/css/App.scss";
import Logo from "../../assets/images/logo.png";
import { history } from "../../history";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      name: ""
    };
  }

  componentDidMount() {
    let userDetails = JSON.parse(localStorage.getItem("user"));
    let { email, name } = userDetails;
    this.setState({
      email,
      name
    });
  }
  logout = () => {
    localStorage.clear();
    history.push("/");
  };
  render() {
    let { email, name } = this.state;
    return (
      <div className="navbar-header dashboard-header clearfix">
        <div className="logo-dashboard">
          <a href="/" id="logo">
            <img className="logo-trans" src={Logo} alt="" />
          </a>
        </div>
        {/*  <div class="tabing">
          <a class="" href="http://accounting.affleprojects.com/dashboard?month=06">CAMPAIGN REPORT</a>
              </div> */}
        <div className="user-profile pull-right">
          <a>
            <span>{name}</span>
            <ul className="dropdown-head-list">
              <li>
                <span>
                  {name}
                  <span />
                </span>
              </li>
              <li>
                {/* <img src="http://accounting.affleprojects.com/images/Mail.png" /> */}
                <span>{email}</span>
              </li>

              {/* 
               <li>
                <img src="http://accounting.affleprojects.com/images/caimpaign.png" />
                <span>Channel Sales</span>
              </li>
              <li>
                <button onclick="window.location='http://accounting.affleprojects.com/logout'">
                  <img src="images/logout.png" />
                  Logout
                </button>
              </li> */}
              <li>
                <button onClick={this.logout}>Logout</button>
              </li>
            </ul>
          </a>
        </div>
      </div>
    );
  }
}

export default Header;
