import React, { Component } from "react";
import "../../assets/css/App.scss";
import Logo from "../../assets/images/logo.png";
import { history } from "../../history";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="dashboardContainer">
        <div className="login-container">
          <div className="login_content">
            <div className="login_left_content">
              <div className="login_left_inner">
                <img src={Logo} alt="company-logo" />
                <h2>
                  Welcome to
                  <br />
                  Affle Accounting Dashboard
                </h2>
                <p>
                  Lorem Ipsum is simply dummy text of the printing &amp;
                  typesetting industry. Lorem Ipsum has been the industry's ever
                  since the 1500s.
                </p>
              </div>
            </div>
            <div className="login_right_content">
              <div className="login_right_inner">
                <div className="login_tabs">
                  <div className="sign_in_inner">
                    <h3>SIGN IN WITH</h3>
                    <div className="google_signin_wrapper">
                      <div className="sign_in_btn">
                        YOLO
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
