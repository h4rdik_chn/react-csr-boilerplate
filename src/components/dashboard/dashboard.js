import React, { Component } from "react";
import "../../assets/css/App.scss";
import editIcon from "../../assets/images/edit.png";
//import { history } from "../../history/history";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="login-container">
        <div className="container-head">
          <h2 className="container_title">Advertiser Reports</h2>
          <div className="campaign_upload_wrapper">
            <div className="add_btn_wrapper">
              <span>
                <b>0</b>/-
              </span>
              <button className="add_cake_btn">Conversion Rate</button>
            </div>
            <div className="quick-campaign">
              {/* <button className="edt_btn">
                <img src={editIcon} />
                Edit &amp; Lock
              </button> */}
              <button className="sub_btn">
                <img src={editIcon} />
                Save &amp; Unlock
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;
