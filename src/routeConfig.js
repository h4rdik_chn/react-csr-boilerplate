/*
    THERE ARE TWO WAYS TO IMPORT
    1. Import directly like the example below
    2. Use dynamic import/loadable-components to implement code splitting

    Note: Use code splitting only when necessary. If the number of routes are significanly lower, 
    then its better to import the components directly as the relative size of the bundle will be small
    enough to load on first render.
*/
import React from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { history } from "./history";
import App from "./components/Home/App";
import Dashboard from "./components/dashboard/dashboard";
import HomeLayoutRoute from "./layouts/blank";
import SideBarLayoutRoute from "./layouts/sidebar";

const Routes = (props) => {
  return (
    <Router history={history}>
      <div className="main_container">
        <Switch>
          <HomeLayoutRoute exact path="/" component={App} authStatus={false} />
          <SideBarLayoutRoute
            exact
            path="/dashboard"
            component={Dashboard}
            authStatus={false}
          />
          <Redirect from="*" to="/" />
        </Switch>
      </div>
    </Router>
  );
};

export default Routes;
