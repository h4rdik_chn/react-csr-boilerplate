import React from 'react';
import App from "../../../src/components/Home/App";
import { shallow } from 'enzyme';

describe("App", () => {

    it("should render correctly", () => {
        const rendered = shallow(<App props={{}} />);
        expect(rendered).toMatchSnapshot();
    });

});