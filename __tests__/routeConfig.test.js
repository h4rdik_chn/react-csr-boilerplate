import React from 'react';
import Routes from "../src/routeConfig";
import { shallow } from 'enzyme';

describe("App", () => {

    it("should render correctly", () => {
        const rendered =  shallow(
            <Routes />
        );
        expect(rendered).toMatchSnapshot();
    });

});