module.exports = {
    displayName: 'components',
    collectCoverageFrom: [
        "**/*.{js,jsx}",
        "!**/coverage/**",
        "!**/node_modules/**",
        "!**/build/**",
        "!**/src/redux/**",
        "!**/src/constants/**",
        "!jest.config.js",
        "!serviceWorker.js",
        "!setupTests.js",
        "**/src/**"
    ],
    snapshotSerializers: [
        "enzyme-to-json/serializer"
    ],
    setupFiles: ["./src/setupTests.js"],
    moduleNameMapper: {
        "\\.(css|scss|less)$": "identity-obj-proxy",
        "\\.(jpg|jpeg|png|svg)$": "jest-transform-stub",
        "^history$": './__mocks__/history.js'
    },
    transform: {
        '^.+\\.js$': 'babel-jest',
        '^history$': './__mocks__/history.js'
    },
    moduleDirectories: [
        ".",
        "src",
        "node_modules"
    ],
    setupFilesAfterEnv: ['./node_modules/jest-enzyme/lib/index.js'],
    // moduleFileExtensions: ["jsx", "js", "json"]
}